\section{Critical data augmentation}

We consider a special case of the supervised learning setting, where we are given a set of input-output pairs $D  = \{(\mathbf{x}_i, \mathbf{y}_i) \in (\mX \times \mY) , \mX \subseteq \mR^d, \mY \subseteq \mR^m, \, i \in \mN_n \}$, sampled i.i.d. according to some unknown probability distribution $p$. We are also given a matrix 
$\mathbf Z: d \times c$, the $i$th row of which, denoted by $\mathbf z_i$, contains a description of the $i$th feature. We call $\mathbf{Z}$ the feature side-information matrix.  Note that matrix $\mathbf Z$ describes intrinsic properties of features. It is drawn from domain knowledge and exists independent of the training instances. We want to learn a function  $\phi : \mX \to \mY$ which minimizes the expected loss $E[L_{\phi}(\phi(x), y)]$, by using both input-output data $D$ as well as the feature side-information matrix $\mathbf{Z}$. We do so by using $\mathbf{Z}$ to augment the training data to generate more data and bring them in the learning. Most importantly, we add a quality check procedure on the augmented data to block those when the augmentation comes from non-relevant, or non-reliable information. We refer to such data augmentation with a quality checking procedure as Critical Data Augmentation (CDA). In the next two sections, we describe how to do the data augmentation with feature side-information and how to impose the quality check process on the augmented data. The critical data augmentation process we introduce here is applicable for both classification and regression tasks. 

\subsection{Data augmentation with feature side-information}\label{transformation}
When we have limited data, we are prone to overfitting, but as the matrix $\mathbf{Z}$ is independent of the training data, we can leverage the knowledge in  $\mathbf{Z}$ to improve the generalization performance. One way to do this is to directly constrain the model by designing a regularizer. The second way is to use such information for augmentation. We follow the second strategy. Each column of the matrix $\mathbf{Z}$ describes the features' intrinsic properties, which means we can measure feature similarity using $\mathbf{Z}$. 

Given two features $i,j$, with side-information vectors $\mathbf z_i, \mathbf z_j$,  the $S_{ij}$ element of $\mathbf S$ is calculated using some similarity function on $\mathbf z_i, \mathbf z_j$. We assume that if two features are similar, they should have near identical effect on the model output \cite{pmlr-v70-mollaysa17a}. This assumption allows us to define a transformation $T: \mathbf{x}, S, \lambda \rightarrow \hat{\mathbf x}$ based on the feature similarity $S$. It will take the existing instance $\mathbf{x}_i \in D$ and transform it to its alternatives $\hat{\mathbf{x}}_i^l \in \mathbf{X}_i^c$, $l\in \{1,2,\dots ,q\}$ which have the same output as $\mathbf{x}_i$. 

We define $T$ as:
\begin{align}
T(\mathbf{x}, s_{ij}, \lambda) = \mathbf x - \lambda \mathbf e_i + \lambda \mathbf e_j.
\end{align}

Let $\mathbf{X}_i^c$ be the set of augmented instances originated from $\mathbf{x}_i$:
\begin{align}
\mathbf{X}_i^c = \{\hat{\mathbf{x}}| \mathbf{x} - \lambda \mathbf e_i + \lambda \mathbf e_j, \lambda\in\Omega, s_{ij}\geq c\},
\end{align}
where $\mathbf e_i$, $\mathbf e_j$, are the $d$-dimensional unit vectors with the $i$th and $j$th dimensions respectively equal to one; $c$ is a constant chosen according to the matrix $S$ such that only those features which have a strong similarity are selected. $\Omega$ is given by data statistics such that it defines the range of the augmentation size so that the generated $\hat{\mathbf{x}}$ feature values stay in the same range as the original $\mathbf{x}$. $\lambda\in \Omega$ is the scalar representing the size of the augmentation.We use the feature similarity matrix to augment the existing training instances by adding a fixed quantity of a feature, while removing the same quantity from its similar pair.If two features we perturbed are similar, then the augmented instance should have the same output as the original instance. In the limit, when two features are identical i.e. $S_{ij} \to \infty$, we can exchange one for another. Concretely, let $i$ and $j$ be such features, We want that:

\begin{align}
\label{eq:nonLinearModelConstraint}
\vect \phi(\mathbf x - \lambda \mathbf e_i + \lambda\mathbf e_j) =
\mathbf y.
\end{align}
Therefore, such a transformation $T$ defines a label preserving (output-invariant in the regression setting) data augmentation, i.e., all generated $\hat{\mathbf x }\in \mathbf{X}_i^c$ have same label $\mathbf y_i$. Then we can train the model with both existing training and augmented data. As we do not know the underlying probability measure $p$, we work with the empirical estimate of the expectation. Therefore, the model we want to solve is:
\begin{align}
\min \sum_{(\mathbf{x}_i, \mathbf{y}_i)\in D}[L_{\phi}(\phi(\mathbf{x}_i), \mathbf{y}_i)+ \gamma \sum_{\hat{\mathbf{x}}_i^l\in \mathbf{X}_i^c}L_{\phi}(\phi(\hat{\mathbf{x}}_i^l), \mathbf{y}_i)].
\label{Initial_model}
\end{align}
where $L$ refers to a standard loss function, $\gamma$ is regularization hyper-parameter.
Note that the similarity matrix $S$ is pre-calculated, independent of the task we are solving. Feature side-information exists independently of our data, which is why it can help to improve generalization performance. However, it means, we are also at the risk of introducing irrelevant information and misleading our model. For instance, when we augment the original instances by perturbing similar feature pairs according to the pre-calculated similarity matrix $S$, some similarity can be irrelevant for the task we are solving. 

Moreover, even if the augmented features are indeed similar pairs, the size of the augmentation also varies depending on where we are in the input space. For instance, if at some point in the input space, the true underlying function is very flat, a big augmentation size will still give us the label preserving effect. Contrarily, at the points where the function is highly sensitive, we can only do a very small perturbation. Remember that we are not in the situation where there are identical features. We only have similar features, which means the invariance of the output is up to a certain augmentation size. Therefore when we generate data using similar feature pairs and different augmentation sizes, we have to do a quality check instance by instance. 

\subsection{Augmented data screening}
Unlike in image classification tasks, we do not have access to the ground truth about how many degrees of rotation or translation will generate label preserving images. Therefore it is hard to asses the quality of the augmented data. In our case, we only have some information, which is independent of the training data, such as the feature side-information which we use to ascertain the feature similarity. However, it is questionable how much this similarity will be true to the learning task and up to which degree this similarity still holds. Therefore it is hard to tell which augmentation is correct and which is wrong. One way to check this is by relying on some ground truth about the data properties. In our case, if two features have the same property, the model should treat them in a similar manner, which means that if feature $i, j$ are similar, and the augmentation size is correct,  then:

\begin{align}
{\vect \phi}^*(\mathbf x) = {\vect \phi}^*(\mathbf x + \lambda\mathbf e_i - \lambda\mathbf e_j) ,
\end{align}

 where ${\vect \phi }^*$ is the true underlying function we want to learn.  This can give us a hint on how to evaluate the quality of the augmented data. However, we do not know the true underlying function $ {\vect \phi}^*$, which produced our data but we can use the training data to get the approximation of ${\vect \phi}^*$:
 
\begin{align}
{\vect \phi}^1 = \operatorname*{argmin}_{\vect \phi} \sum_{(\mathbf{x}_i, \mathbf{y}_i)\in D}L_{\phi}(\phi(\mathbf{x}_i), \mathbf{y}_i) + \gamma R_{\phi}(\phi),
\label{condition}
\end{align}
where $\gamma \in \mR_{\geq 0 }$ is the regularization hyper-parameter and $R_{\phi} : {\phi} \to  \mR_{\geq 0 }$ is a standard regularizer as $L2$

Then we can use ${\vect \phi}^1$ to check how the model reacts to the perturbation of the similar features. If the two features are truly similar, and the augmentation size is correct, then the model should have similar response on the original and augmented data. Therefore, we can use a relaxed constraint to check the quality of the augmented data. Define the distance:

\begin{align}
 d = ||\vect \phi^1(\mathbf x) - {\vect \phi}^1(\mathbf x + \lambda\mathbf e_i - \lambda \mathbf e_j) ||.
\end{align}
 If $d \leq \epsilon$ for some threshold parameter $\epsilon $, which will be tuned during learning, the augmentation is considered valid. In contrast, if $d \geq \epsilon$, this implies that feature $i,j$ have very different effects on the model, and thus should not be considered similar. Consequently, the resulting augmentation is filtered out. The final model is thus defined as:
 
\begin{align}
\begin{split}
 &\min_{\vect \phi}\sum_{(\mathbf{x}_i, \mathbf{y}_i)\in D}[L_{\phi}(\phi(\mathbf{x}_i), \mathbf{y}_i)+ \\
& \gamma  \sum_{\hat{\mathbf{x}}_i^l\in \mathbf{X}_i^c}\frac{1}{\epsilon - d_i^l}\textit{ReLu}((\epsilon - d_i^l)L_{\phi}(\vect \phi (\hat{\mathbf{x}}_i^l), \mathbf y_i) )],
\label{Final_model}
\end{split}
\end{align}
where $d_i^l = ||{\vect \phi}^1(\mathbf x_i) - {\vect \phi}^1(\hat{\mathbf x}_i) || $, ${\vect \phi}^1 $ is defined as in equation \ref{condition}. Objective \ref{Final_model} is comprised of two parts. The first part is the loss on the existing training data, while the second part is the loss on the augmented data.  For each training data pair ${(\mathbf{x}_i, \mathbf{y}_i)\in D}$, we generate a set of its augmentations $\mathbf{X}_i^c =\{(\hat{\mathbf{x}}_i^l, \mathbf{y}_i), \:\: l = 1, 2, 3 \dots L\}$, where $\hat{\mathbf{x}}_i ^l\in \mathbf{X}_i^c$. For each of the generated $(\hat{\mathbf{x}}_i^l, \mathbf{y}_i)$, we use ${\vect \phi}^1 $  to determine $d_i^l = ||{\vect \phi}^1(\mathbf x_i) - {\vect \phi}^1(\hat{\mathbf x}_i) || $. If $d_i^l  \leq \epsilon$, the second part of the objective corresponding to this generated instance will become $\gamma L_{\phi}(\vect \phi (\hat{\mathbf{x}}_i^l), \mathbf y_i) $. Contrarily, if $d_i^l >\epsilon$, then the second part of the objective regarding this augmentation will become 0, which means we discard this specific augmentation from the objective. 

As a result, we train the model with existing training and with the augmented data, whilst blocking the wrong augmentation. During optimization, we can use $\phi^1$ as initialization, and as for $\epsilon$, we use the statistics of $d_i^l = ||{\vect \phi}^1(\mathbf x_i) - {\vect \phi}^1(\hat{\mathbf x}_i) || $ on the validation to set a range of values, where $\epsilon$ can be tuned. Notice that if the best value of $\epsilon$ happens at $\epsilon = inf$, it means all the augmentation is correct and we include all of them in the training. This also informs us that the $S$ matrix is reliable and the size of augmentation is correct. In this case the model  \ref{Final_model} and model \ref{Initial_model} are equivalent. If $\epsilon =0$ on the other hand, it implies that all the augmentation is not valid and we revert back to model \ref{condition}.
