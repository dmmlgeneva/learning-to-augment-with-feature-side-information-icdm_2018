\section{Introduction}
\label{Introduction}

Neural networks typically need huge amounts of data to train in order to get reasonable results. One way of solving this is to artificially generate more data, by using some prior knowledge of the data properties or other relevant domain knowledge. However, if the assumptions on the data properties are not accurate or the domain knowledge is irrelevant to the task at hand, then we might end up degenerating the learning performance by using such augmented data in comparison to simply training on the limited available dataset. Therefore, the natural questions to ask are: What are safe ways of doing data augmentation? How to avoid incorporating misleading augmented data during learning? 

Even though data augmentation is used widely, there is surprisingly limited work done that examines the safety of the augmented data with respect to the learning task.In this paper, we focus on learning a safe data augmentation process by using side-information in the case when we are not sure which part of the available information, i.e. prior, side-information, is reliable and which part is not. Side information is the data that is neither from the input space nor from the output space of the function but includes
useful information for learning. \cite{jonschkowski2015patterns}.


We will consider learning problems in which we have so-called feature side-information, which describes the properties and/or the relations of the features. These are feature intrinsic properties and we can use those to define a feature similarity matrix $\mathbf{S}$. Our main assumption is that two similar features should have a similar effect on the model output.  In the limit, if two features are identical, they are exchangeable \cite{pmlr-v70-mollaysa17a}. By following this assumption we can augment the existing data to generate new label preserving (output invariant in a regression setting) instances. Then we can train the model on both existing training data as well as the newly generated data. 


However, how much we can gain by incorporating the augmented data in the model, completely depends on the quality of the augmented data, which in turn depends on the credibility of the prior knowledge we used when we did the data augmentation.
What if the prior knowledge is not fully reliable? e.g., part of the similarity matrix can be wrong due to the fact that not every dimension of the feature side-information is relevant, or the similarity measure we use is not a proper measure with respect to the learning task. The main question we want to address is when we do data augmentation, how we can filter out augmented data which possibly originated from using false or irrelevant information. One way of solving this problem is to learn a similarity measure directly inside the model, instead of calculating it independently of the learning phase. The second way is to use the pre-calculated similarity matrix in a critical way, such that we prevent the wrongly augmented data from entering the model, when we do not have complete knowledge on how to optimally do the augmentation. 

In this paper we present the second approach. It is implemented by applying a quality checking process on the augmented data, instance by instance. It will let the correctly augmented data enter the model, whilst blocking the rest. This extra learning function will enable us to be able to safely and more critically make use of the available side-information, whilst generating more reliable data. Notice that in this work we focus on a very specific transformation to do the augmentation. However, the quality checking procedure is not restricted to such a specific transformation. It is applicable to a wide range of transformations. For example, in the MNIST dataset, if we define the transformation as the rotation of an image, our model can be used with arbitrary rotations of the image, without specifying the degree of the rotation. The model itself will take care of preventing the $90^{\circ}$ rotated 6 or 9 of entering the model.







